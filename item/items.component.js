"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var observable_array_1 = require("tns-core-modules/data/observable-array");
var page_1 = require("ui/page");
var color_1 = require("color");
var item_service_1 = require("./item.service");
var ItemsComponent = (function () {
    function ItemsComponent(itemService, page) {
        this.itemService = itemService;
        this.page = page;
        this.show = false;
        this.showAnim = true;
        this.limit = 290;
    }
    ItemsComponent.prototype.ngOnInit = function () {
        this.page.actionBarHidden = true;
        this.page.backgroundColor = new color_1.Color("#e0e0e0");
        this.items = new observable_array_1.ObservableArray(this.itemService.getItems());
        console.log("Items Loaded");
    };
    ItemsComponent.prototype.onScroll = function (args) {
        this.status = "scrolling";
        var searchBox = this.searchBox.nativeElement;
        var searchAction = this.searchAction.nativeElement;
        var that = this;
        setTimeout(function () {
            that.status = "not scrolling";
        }, 300);
        if (args.scrollY >= this.limit) {
            this.show = true;
            if (this.showAnim) {
                searchAction.animate({
                    opacity: 0,
                    duration: 0
                });
                searchAction.animate({
                    opacity: 1,
                    duration: 400
                });
                this.showAnim = false;
            }
            searchBox.animate({
                opacity: 0,
                duration: 400
            });
            console.log("Showing...");
        }
        else if (args.scrollY <= this.limit) {
            this.show = false;
            if (!this.showAnim) {
                searchAction.animate({
                    opacity: 0,
                    duration: 0
                });
                searchAction.animate({
                    opacity: 1,
                    duration: 400
                });
                this.showAnim = true;
            }
            searchBox.animate({
                opacity: 1,
                duration: 400
            });
            console.log("Hiding Textfield...");
        }
        console.log("scrollX: " + args.scrollX);
        console.log("scrollY: " + args.scrollY);
    };
    return ItemsComponent;
}());
__decorate([
    core_1.ViewChild("searchBox"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "searchBox", void 0);
__decorate([
    core_1.ViewChild("searchAction"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "searchAction", void 0);
ItemsComponent = __decorate([
    core_1.Component({
        selector: "ns-items",
        moduleId: module.id,
        templateUrl: "items.component.html",
        styleUrls: ["items.component.css"]
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService, page_1.Page])
], ItemsComponent);
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXlFO0FBQ3pFLDJFQUFrRztBQUVsRyxnQ0FBK0I7QUFFL0IsK0JBQTZCO0FBSTdCLCtDQUE2QztBQVE3QyxJQUFhLGNBQWM7SUFTdkIsd0JBQW9CLFdBQXdCLEVBQVUsSUFBVTtRQUE1QyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFVLFNBQUksR0FBSixJQUFJLENBQU07UUFDNUQsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUM7UUFDbEIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7UUFDckIsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUM7SUFDckIsQ0FBQztJQUVELGlDQUFRLEdBQVI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxhQUFLLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGtDQUFlLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO1FBQzlELE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDaEMsQ0FBQztJQUVNLGlDQUFRLEdBQWYsVUFBZ0IsSUFBcUI7UUFDakMsSUFBSSxDQUFDLE1BQU0sR0FBRyxXQUFXLENBQUM7UUFDMUIsSUFBSSxTQUFTLEdBQVUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUM7UUFDcEQsSUFBSSxZQUFZLEdBQVUsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7UUFDMUQsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNQLElBQUksQ0FBQyxNQUFNLEdBQUcsZUFBZSxDQUFDO1FBQ2xDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNSLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7WUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2hCLFlBQVksQ0FBQyxPQUFPLENBQUM7b0JBQ2pCLE9BQU8sRUFBRSxDQUFDO29CQUNWLFFBQVEsRUFBRSxDQUFDO2lCQUNkLENBQUMsQ0FBQztnQkFDSCxZQUFZLENBQUMsT0FBTyxDQUFDO29CQUNqQixPQUFPLEVBQUUsQ0FBQztvQkFDVixRQUFRLEVBQUUsR0FBRztpQkFDaEIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1lBQzFCLENBQUM7WUFDRCxTQUFTLENBQUMsT0FBTyxDQUFDO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2dCQUNWLFFBQVEsRUFBRSxHQUFHO2FBQ2hCLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDOUIsQ0FBQztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDO1lBQ2xCLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQ2pCLFlBQVksQ0FBQyxPQUFPLENBQUM7b0JBQ2pCLE9BQU8sRUFBRSxDQUFDO29CQUNWLFFBQVEsRUFBRSxDQUFDO2lCQUNkLENBQUMsQ0FBQztnQkFDSCxZQUFZLENBQUMsT0FBTyxDQUFDO29CQUNqQixPQUFPLEVBQUUsQ0FBQztvQkFDVixRQUFRLEVBQUUsR0FBRztpQkFDaEIsQ0FBQyxDQUFDO2dCQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3pCLENBQUM7WUFDRCxTQUFTLENBQUMsT0FBTyxDQUFDO2dCQUNkLE9BQU8sRUFBRSxDQUFDO2dCQUNWLFFBQVEsRUFBRSxHQUFHO2FBQ2hCLENBQUMsQ0FBQztZQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUN2QyxDQUFDO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQUFDLEFBdEVELElBc0VDO0FBaEUyQjtJQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQzs4QkFBWSxpQkFBVTtpREFBQztBQUNuQjtJQUExQixnQkFBUyxDQUFDLGNBQWMsQ0FBQzs4QkFBZSxpQkFBVTtvREFBQztBQVAzQyxjQUFjO0lBTjFCLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsVUFBVTtRQUNwQixRQUFRLEVBQUUsTUFBTSxDQUFDLEVBQUU7UUFDbkIsV0FBVyxFQUFFLHNCQUFzQjtRQUNuQyxTQUFTLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQztLQUNyQyxDQUFDO3FDQVVtQywwQkFBVyxFQUFnQixXQUFJO0dBVHZELGNBQWMsQ0FzRTFCO0FBdEVZLHdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlQXJyYXksIENoYW5nZWREYXRhLCBDaGFuZ2VUeXBlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlLWFycmF5XCI7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tIFwicnhqcy9CZWhhdmlvclN1YmplY3RcIjtcbmltcG9ydCB7IFBhZ2UgfSBmcm9tIFwidWkvcGFnZVwiO1xuaW1wb3J0IHsgVmlldyB9IGZyb20gXCJ1aS9jb3JlL3ZpZXdcIjtcbmltcG9ydCB7IENvbG9yIH0gZnJvbVwiY29sb3JcIjtcbmltcG9ydCB7IFNjcm9sbEV2ZW50RGF0YSB9IGZyb20gXCJ1aS9zY3JvbGwtdmlld1wiO1xuXG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vaXRlbVwiO1xuaW1wb3J0IHsgSXRlbVNlcnZpY2UgfSBmcm9tIFwiLi9pdGVtLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtaXRlbXNcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIml0ZW1zLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCJpdGVtcy5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIEl0ZW1zQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgICBpdGVtczogT2JzZXJ2YWJsZUFycmF5PEl0ZW0+O1xuICAgIHN0YXR1czogc3RyaW5nO1xuICAgIHNob3c6IGJvb2xlYW47XG4gICAgbGltaXQ6IG51bWJlcjtcbiAgICBzaG93QW5pbTogYm9vbGVhbjtcbiAgICBAVmlld0NoaWxkKFwic2VhcmNoQm94XCIpIHNlYXJjaEJveDogRWxlbWVudFJlZjtcbiAgICBAVmlld0NoaWxkKFwic2VhcmNoQWN0aW9uXCIpIHNlYXJjaEFjdGlvbjogRWxlbWVudFJlZjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaXRlbVNlcnZpY2U6IEl0ZW1TZXJ2aWNlLCBwcml2YXRlIHBhZ2U6IFBhZ2UpIHtcbiAgICAgICAgdGhpcy5zaG93ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2hvd0FuaW0gPSB0cnVlO1xuICAgICAgICB0aGlzLmxpbWl0ID0gMjkwO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLnBhZ2UuYWN0aW9uQmFySGlkZGVuID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5wYWdlLmJhY2tncm91bmRDb2xvciA9IG5ldyBDb2xvcihcIiNlMGUwZTBcIik7XG4gICAgICAgIHRoaXMuaXRlbXMgPSBuZXcgT2JzZXJ2YWJsZUFycmF5KHRoaXMuaXRlbVNlcnZpY2UuZ2V0SXRlbXMoKSk7XG4gICAgICAgIGNvbnNvbGUubG9nKFwiSXRlbXMgTG9hZGVkXCIpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvblNjcm9sbChhcmdzOiBTY3JvbGxFdmVudERhdGEpIHtcbiAgICAgICAgdGhpcy5zdGF0dXMgPSBcInNjcm9sbGluZ1wiO1xuICAgICAgICBsZXQgc2VhcmNoQm94ID0gPFZpZXc+IHRoaXMuc2VhcmNoQm94Lm5hdGl2ZUVsZW1lbnQ7XG4gICAgICAgIGxldCBzZWFyY2hBY3Rpb24gPSA8Vmlldz4gdGhpcy5zZWFyY2hBY3Rpb24ubmF0aXZlRWxlbWVudDtcbiAgICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHRoYXQuc3RhdHVzID0gXCJub3Qgc2Nyb2xsaW5nXCI7XG4gICAgICAgIH0sIDMwMCk7XG4gICAgICAgIGlmIChhcmdzLnNjcm9sbFkgPj0gdGhpcy5saW1pdCkge1xuICAgICAgICAgICAgdGhpcy5zaG93ID0gdHJ1ZTtcbiAgICAgICAgICAgIGlmICh0aGlzLnNob3dBbmltKSB7XG4gICAgICAgICAgICAgICAgc2VhcmNoQWN0aW9uLmFuaW1hdGUoe1xuICAgICAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgICAgICBkdXJhdGlvbjogMFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHNlYXJjaEFjdGlvbi5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMSxcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDQwMFxuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIHRoaXMuc2hvd0FuaW0gPSBmYWxzZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlYXJjaEJveC5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAwLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiA0MDBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJTaG93aW5nLi4uXCIpO1xuICAgICAgICB9IGVsc2UgaWYgKGFyZ3Muc2Nyb2xsWSA8PSB0aGlzLmxpbWl0KSB7XG4gICAgICAgICAgICB0aGlzLnNob3cgPSBmYWxzZTtcbiAgICAgICAgICAgIGlmICghdGhpcy5zaG93QW5pbSkge1xuICAgICAgICAgICAgICAgIHNlYXJjaEFjdGlvbi5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICAgICAgb3BhY2l0eTogMCxcbiAgICAgICAgICAgICAgICAgICAgZHVyYXRpb246IDBcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICBzZWFyY2hBY3Rpb24uYW5pbWF0ZSh7XG4gICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6IDEsXG4gICAgICAgICAgICAgICAgICAgIGR1cmF0aW9uOiA0MDBcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB0aGlzLnNob3dBbmltID0gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHNlYXJjaEJveC5hbmltYXRlKHtcbiAgICAgICAgICAgICAgICBvcGFjaXR5OiAxLFxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiA0MDBcbiAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJIaWRpbmcgVGV4dGZpZWxkLi4uXCIpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKFwic2Nyb2xsWDogXCIgKyBhcmdzLnNjcm9sbFgpO1xuICAgICAgICBjb25zb2xlLmxvZyhcInNjcm9sbFk6IFwiICsgYXJncy5zY3JvbGxZKTtcbiAgICB9XG59XG4iXX0=