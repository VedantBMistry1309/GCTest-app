"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ItemService = (function () {
    function ItemService() {
        this.items = new Array({ id: 1, name: "Ter Stegen", role: "Goalkeeper", src: "http://images.performgroup.com/di/library/goal_es/ab/d9/marc-andre-ter-stegen-barcelona-atletico-madrid-la-liga_9aaiu7wl2amh1mdgze6xh04gf.jpg?t=1318343271&w=620&h=430" }, { id: 3, name: "Piqué", role: "Defender", src: "http://images.performgroup.com/di/library/omnisport/aa/d1/gerard-pique-cropped_3y2xyriyhrae17nz0ntdqsraf.jpg?t=-13063301&w=620&h=430" }, { id: 4, name: "I. Rakitic", role: "Midfielder", src: "http://i2.mirror.co.uk/incoming/article8887608.ece/ALTERNATES/s615/FC-Barcelona-v-Club-Atletico-de-Madrid-La-Liga.jpg" }, { id: 5, name: "Sergio", role: "Midfielder", src: "http://i.dailymail.co.uk/i/pix/2015/06/29/14/29F5CC7900000578-0-image-a-2_1435584845697.jpg" }, { id: 6, name: "Denis Suárez", role: "Midfielder", src: "http://a.espncdn.com/combiner/i/?img=/photo/2017/0126/r175419_1296x729_16-9.jpg&w=738&site=espnfc" }, { id: 7, name: "Arda", role: "Midfielder", src: "https://www.thesun.co.uk/wp-content/uploads/2016/08/nintchdbpict0002555935391.jpg?w=960&strip=all" }
        /*
        { id: 8, name: "A. Iniesta", role: "Midfielder" },
        { id: 9, name: "Suárez", role: "Forward" },
        { id: 10, name: "Messi", role: "Forward" },
        { id: 11, name: "Neymar", role: "Forward" },
        { id: 12, name: "Rafinha", role: "Midfielder" },
        { id: 13, name: "Cillessen", role: "Goalkeeper" },
        { id: 14, name: "Mascherano", role: "Defender" },
        { id: 17, name: "Paco Alcácer", role: "Forward" },
        { id: 18, name: "Jordi Alba", role: "Defender" },
        { id: 19, name: "Digne", role: "Defender" },
        { id: 20, name: "Sergi Roberto", role: "Midfielder" },
        { id: 21, name: "André Gomes", role: "Midfielder" },
        { id: 22, name: "Aleix Vidal", role: "Midfielder" },
        { id: 23, name: "Umtiti", role: "Defender" },
        { id: 24, name: "Mathieu", role: "Defender" },
        { id: 25, name: "Masip", role: "Goalkeeper" }
        */
        );
    }
    ItemService.prototype.getItems = function () {
        return this.items;
    };
    ItemService.prototype.getItem = function (id) {
        return this.items.filter(function (item) { return item.id === id; })[0];
    };
    return ItemService;
}());
ItemService = __decorate([
    core_1.Injectable()
], ItemService);
exports.ItemService = ItemService;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQTJDO0FBSzNDLElBQWEsV0FBVztJQUR4QjtRQUVZLFVBQUssR0FBRyxJQUFJLEtBQUssQ0FDckIsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsd0tBQXdLLEVBQUUsRUFDaE8sRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsc0lBQXNJLEVBQUUsRUFDdkwsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsdUhBQXVILEVBQUUsRUFDL0ssRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsNkZBQTZGLEVBQUUsRUFDakosRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsbUdBQW1HLEVBQUUsRUFDN0osRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRSxHQUFHLEVBQUUsbUdBQW1HLEVBQUU7UUFDcko7Ozs7Ozs7Ozs7Ozs7Ozs7O1VBaUJFO1NBQ0wsQ0FBQztJQVNOLENBQUM7SUFQRyw4QkFBUSxHQUFSO1FBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQztJQUVELDZCQUFPLEdBQVAsVUFBUSxFQUFVO1FBQ2QsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQWQsQ0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDeEQsQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQW5DRCxJQW1DQztBQW5DWSxXQUFXO0lBRHZCLGlCQUFVLEVBQUU7R0FDQSxXQUFXLENBbUN2QjtBQW5DWSxrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vaXRlbVwiO1xuXG5ASW5qZWN0YWJsZSgpXG5leHBvcnQgY2xhc3MgSXRlbVNlcnZpY2Uge1xuICAgIHByaXZhdGUgaXRlbXMgPSBuZXcgQXJyYXk8SXRlbT4oXG4gICAgICAgIHsgaWQ6IDEsIG5hbWU6IFwiVGVyIFN0ZWdlblwiLCByb2xlOiBcIkdvYWxrZWVwZXJcIiwgc3JjOiBcImh0dHA6Ly9pbWFnZXMucGVyZm9ybWdyb3VwLmNvbS9kaS9saWJyYXJ5L2dvYWxfZXMvYWIvZDkvbWFyYy1hbmRyZS10ZXItc3RlZ2VuLWJhcmNlbG9uYS1hdGxldGljby1tYWRyaWQtbGEtbGlnYV85YWFpdTd3bDJhbWgxbWRnemU2eGgwNGdmLmpwZz90PTEzMTgzNDMyNzEmdz02MjAmaD00MzBcIiB9LFxuICAgICAgICB7IGlkOiAzLCBuYW1lOiBcIlBpcXXDqVwiLCByb2xlOiBcIkRlZmVuZGVyXCIsIHNyYzogXCJodHRwOi8vaW1hZ2VzLnBlcmZvcm1ncm91cC5jb20vZGkvbGlicmFyeS9vbW5pc3BvcnQvYWEvZDEvZ2VyYXJkLXBpcXVlLWNyb3BwZWRfM3kyeHlyaXlocmFlMTduejBudGRxc3JhZi5qcGc/dD0tMTMwNjMzMDEmdz02MjAmaD00MzBcIiB9LFxuICAgICAgICB7IGlkOiA0LCBuYW1lOiBcIkkuIFJha2l0aWNcIiwgcm9sZTogXCJNaWRmaWVsZGVyXCIsIHNyYzogXCJodHRwOi8vaTIubWlycm9yLmNvLnVrL2luY29taW5nL2FydGljbGU4ODg3NjA4LmVjZS9BTFRFUk5BVEVTL3M2MTUvRkMtQmFyY2Vsb25hLXYtQ2x1Yi1BdGxldGljby1kZS1NYWRyaWQtTGEtTGlnYS5qcGdcIiB9LFxuICAgICAgICB7IGlkOiA1LCBuYW1lOiBcIlNlcmdpb1wiLCByb2xlOiBcIk1pZGZpZWxkZXJcIiwgc3JjOiBcImh0dHA6Ly9pLmRhaWx5bWFpbC5jby51ay9pL3BpeC8yMDE1LzA2LzI5LzE0LzI5RjVDQzc5MDAwMDA1NzgtMC1pbWFnZS1hLTJfMTQzNTU4NDg0NTY5Ny5qcGdcIiB9LFxuICAgICAgICB7IGlkOiA2LCBuYW1lOiBcIkRlbmlzIFN1w6FyZXpcIiwgcm9sZTogXCJNaWRmaWVsZGVyXCIsIHNyYzogXCJodHRwOi8vYS5lc3BuY2RuLmNvbS9jb21iaW5lci9pLz9pbWc9L3Bob3RvLzIwMTcvMDEyNi9yMTc1NDE5XzEyOTZ4NzI5XzE2LTkuanBnJnc9NzM4JnNpdGU9ZXNwbmZjXCIgfSxcbiAgICAgICAgeyBpZDogNywgbmFtZTogXCJBcmRhXCIsIHJvbGU6IFwiTWlkZmllbGRlclwiLCBzcmM6IFwiaHR0cHM6Ly93d3cudGhlc3VuLmNvLnVrL3dwLWNvbnRlbnQvdXBsb2Fkcy8yMDE2LzA4L25pbnRjaGRicGljdDAwMDI1NTU5MzUzOTEuanBnP3c9OTYwJnN0cmlwPWFsbFwiIH1cbiAgICAgICAgLypcbiAgICAgICAgeyBpZDogOCwgbmFtZTogXCJBLiBJbmllc3RhXCIsIHJvbGU6IFwiTWlkZmllbGRlclwiIH0sXG4gICAgICAgIHsgaWQ6IDksIG5hbWU6IFwiU3XDoXJlelwiLCByb2xlOiBcIkZvcndhcmRcIiB9LFxuICAgICAgICB7IGlkOiAxMCwgbmFtZTogXCJNZXNzaVwiLCByb2xlOiBcIkZvcndhcmRcIiB9LFxuICAgICAgICB7IGlkOiAxMSwgbmFtZTogXCJOZXltYXJcIiwgcm9sZTogXCJGb3J3YXJkXCIgfSxcbiAgICAgICAgeyBpZDogMTIsIG5hbWU6IFwiUmFmaW5oYVwiLCByb2xlOiBcIk1pZGZpZWxkZXJcIiB9LFxuICAgICAgICB7IGlkOiAxMywgbmFtZTogXCJDaWxsZXNzZW5cIiwgcm9sZTogXCJHb2Fsa2VlcGVyXCIgfSxcbiAgICAgICAgeyBpZDogMTQsIG5hbWU6IFwiTWFzY2hlcmFub1wiLCByb2xlOiBcIkRlZmVuZGVyXCIgfSxcbiAgICAgICAgeyBpZDogMTcsIG5hbWU6IFwiUGFjbyBBbGPDoWNlclwiLCByb2xlOiBcIkZvcndhcmRcIiB9LFxuICAgICAgICB7IGlkOiAxOCwgbmFtZTogXCJKb3JkaSBBbGJhXCIsIHJvbGU6IFwiRGVmZW5kZXJcIiB9LFxuICAgICAgICB7IGlkOiAxOSwgbmFtZTogXCJEaWduZVwiLCByb2xlOiBcIkRlZmVuZGVyXCIgfSxcbiAgICAgICAgeyBpZDogMjAsIG5hbWU6IFwiU2VyZ2kgUm9iZXJ0b1wiLCByb2xlOiBcIk1pZGZpZWxkZXJcIiB9LFxuICAgICAgICB7IGlkOiAyMSwgbmFtZTogXCJBbmRyw6kgR29tZXNcIiwgcm9sZTogXCJNaWRmaWVsZGVyXCIgfSxcbiAgICAgICAgeyBpZDogMjIsIG5hbWU6IFwiQWxlaXggVmlkYWxcIiwgcm9sZTogXCJNaWRmaWVsZGVyXCIgfSxcbiAgICAgICAgeyBpZDogMjMsIG5hbWU6IFwiVW10aXRpXCIsIHJvbGU6IFwiRGVmZW5kZXJcIiB9LFxuICAgICAgICB7IGlkOiAyNCwgbmFtZTogXCJNYXRoaWV1XCIsIHJvbGU6IFwiRGVmZW5kZXJcIiB9LFxuICAgICAgICB7IGlkOiAyNSwgbmFtZTogXCJNYXNpcFwiLCByb2xlOiBcIkdvYWxrZWVwZXJcIiB9XG4gICAgICAgICovXG4gICAgKTtcblxuICAgIGdldEl0ZW1zKCk6IEl0ZW1bXSB7XG4gICAgICAgIHJldHVybiB0aGlzLml0ZW1zO1xuICAgIH1cblxuICAgIGdldEl0ZW0oaWQ6IG51bWJlcik6IEl0ZW0ge1xuICAgICAgICByZXR1cm4gdGhpcy5pdGVtcy5maWx0ZXIoaXRlbSA9PiBpdGVtLmlkID09PSBpZClbMF07XG4gICAgfVxufVxuIl19